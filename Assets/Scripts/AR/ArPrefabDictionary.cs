﻿using System;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;
using UnityEngine.Serialization;
#if UNITY_EDITOR
using UnityEditor;

#endif
namespace AR
{
    [Serializable]
    public class BuildingPrefabDictionary : SerializableDictionaryBase<BuildingType, GameObject>
    {
    }

    [Serializable]
    public class RoadPrefabDictionary : SerializableDictionaryBase<RoadType, GameObject>
    {
    }

    //[CreateAssetMenu(fileName = "ArPrefabDictionary")] 
    public class ArPrefabDictionary : ScriptableObject
    {
        public GameObject defaultBuildingPrefab;
        public GameObject defaultRoadPrefab;
        public BuildingPrefabDictionary buildingPrefabs = new BuildingPrefabDictionary();
        public RoadPrefabDictionary roadPrefabs = new RoadPrefabDictionary();
    }
#if UNITY_EDITOR
    [CustomEditor(typeof(ArPrefabDictionary))]
    public class ArLogicEditor : Editor
    {
        private ArPrefabDictionary _target;

        private void OnEnable()
        {
            _target = target as ArPrefabDictionary;
            Debug.Assert(_target != null, nameof(_target) + " != null");
            if (_target.buildingPrefabs == null)
                _target.buildingPrefabs = new BuildingPrefabDictionary();
        }

        private void InitializeBuildingDictionary()
        {
            foreach (BuildingType buildingName in Enum.GetValues(typeof(BuildingType)))
                if (!_target.buildingPrefabs.ContainsKey(buildingName))
                    _target.buildingPrefabs.Add(buildingName, _target.defaultBuildingPrefab);
        }

        private void ClearBuildingDictionary()
        {
            _target.buildingPrefabs = new BuildingPrefabDictionary();
        }

        private void InitializeRoadDictionary()
        {
            foreach (RoadType roadName in Enum.GetValues(typeof(RoadType)))
                if (!_target.roadPrefabs.ContainsKey(roadName))
                    _target.roadPrefabs.Add(roadName, _target.defaultRoadPrefab);
        }

        private void ClearRoadDictionary()
        {
            _target.roadPrefabs = new RoadPrefabDictionary();
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            EditorGUILayout.Separator();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Buildings : ");
            if (GUILayout.Button("Initialize Buildings"))
                InitializeBuildingDictionary();
            if (GUILayout.Button("Clear Buildings"))
                ClearBuildingDictionary();
            GUILayout.EndHorizontal();
            EditorGUILayout.Separator();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Roads : ");
            if (GUILayout.Button("Initialize Roads"))
                InitializeRoadDictionary();
            if (GUILayout.Button("Clear Roads"))
                ClearRoadDictionary();
            GUILayout.EndHorizontal();
        }
    }
#endif
}