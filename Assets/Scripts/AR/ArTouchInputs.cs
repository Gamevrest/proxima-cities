﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace AR
{
    public class ArTouchInputs : MonoBehaviour
    {
        private enum TouchLocation
        {
            NOWHERE,
            LEFT,
            RIGHT,
            MIDDLE
        }

        public ArManager arManager;
        [Tooltip("USE VIEWPORT VALUE")] public float leftArea;
        private Rect _leftAreaRect;
        [Tooltip("USE VIEWPORT VALUE")] public float rightArea;
        private Rect _rightAreaRect;
        private Rect _middleAreaRect;
        public float scaleSpeed;
        public float rotationSpeed;
        private TouchLocation _previousLocation;
        private float _coef;

        private void Start()
        {
            var screenResolution = Screen.currentResolution;
            _coef = 100.0f / screenResolution.height;
            var la = leftArea * screenResolution.width;
            _leftAreaRect = new Rect(0, 0, la, screenResolution.height);
            var ra = rightArea * screenResolution.width;
            var rra = screenResolution.width - ra;
            _rightAreaRect = new Rect(rra, 0, ra, screenResolution.height);
            _middleAreaRect = new Rect(la, 0, rra, screenResolution.height);
        }


        private void Update()
        {
            CheckTouches();
        }

        private void CheckTouches()
        {
            if (Input.touchCount <= 0) return;
            var touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    _previousLocation = CheckWhereTouchIs();
                    break;
                case TouchPhase.Moved:
                case TouchPhase.Ended:
                case TouchPhase.Canceled:
                    ModifCity(touch);
                    break;
            }
        }

        private void ModifCity(Touch touch)
        {
            var curLocation = CheckWhereTouchIs();

            if (curLocation != _previousLocation)
                return;
            switch (_previousLocation)
            {
                case TouchLocation.LEFT:
                    arManager.ZoomCity(_coef * touch.deltaPosition.y * scaleSpeed);
                    break;
                case TouchLocation.RIGHT:
                    arManager.RotateCity(_coef * touch.deltaPosition.y * rotationSpeed);
                    break;
                case TouchLocation.MIDDLE:
                    arManager.DropCity();
                    break;
            }
        }

        private TouchLocation CheckWhereTouchIs()
        {
            return Input.touchCount <= 0 ? TouchLocation.NOWHERE :
                IsContaining(_leftAreaRect) ? TouchLocation.LEFT :
                IsContaining(_rightAreaRect) ? TouchLocation.RIGHT :
                IsContaining(_middleAreaRect) ? TouchLocation.MIDDLE :
                TouchLocation.NOWHERE;
        }

        private bool IsContaining(Rect rect) => rect.Contains(Input.touches[0].position);
    }
}