﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AR
{
    public class ArInstructions : MonoBehaviour
    {
        public GameObject maison;
        public float delay;
        [ReadOnly] public bool delaying;
        [ReadOnly] public int index;
        public List<GameObject> tuto;

        private NewArTouchInputs _inputs;

        private void OnEnable()
        {
            maison.SetActive(true);
            delaying = false;
            _inputs = GetComponent<NewArTouchInputs>();
            _inputs.gesturesActivated = true;
            index = -1;
            NextTuto();
        }

        public void NextTuto()
        {
            index++;
            for (var i = 0; i < tuto.Count; i++)
            {
                tuto[i].SetActive(i == index);
            }

            if (index >= tuto.Count)
            {
                maison.SetActive(false);
                _inputs.gesturesActivated = true;
            }
        }
        
        private void Update()
        {
            if (!delaying && (Input.touchCount > 0 || Input.anyKey))
            {
                NextTuto();
                StartCoroutine(DelaySkip());
            }
        }

        private IEnumerator DelaySkip()
        {
            delaying = true;
            yield return  new WaitForSeconds(delay);
            delaying = false;
        }
    }
}