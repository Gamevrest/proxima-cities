﻿using System;
using System.Globalization;
using Tools;
using UnityEngine;
using UnityEngine.Serialization;

namespace AR
{
    public class NewArTouchInputs : MonoBehaviour
    {
        public ArManager arManager;
        public float zoomSpeed;
        public float rotationSpeed;
        public float maxDoubleTapTime;
        public TouchMovementCalc calc;

        public bool gesturesActivated;

        private int _tapCount;
        private float _newTime;

        private void LateUpdate()
        {
            calc.Calculate();
            DevConsole.Log(calc.pinchDistanceDelta.ToString(CultureInfo.InvariantCulture), "pinch");
            DevConsole.Log(calc.turnAngleDelta.ToString(CultureInfo.InvariantCulture), "turn");
            if (Mathf.Abs(calc.pinchDistanceDelta) > 0)
                arManager.ZoomCity(calc.pinchDistanceDelta * zoomSpeed);
            if (Mathf.Abs(calc.turnAngleDelta) > 0)
                arManager.RotateCity(calc.turnAngleDelta * rotationSpeed);
            CheckDropCity();
        }

        private void CheckDropCity()
        {
            if (Input.touchCount == 1)
            {
                var touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Ended)
                    _tapCount++;
                switch (_tapCount)
                {
                    case 1:
                        _newTime = Time.time + maxDoubleTapTime;
                        break;
                    case 2 when Time.time <= _newTime:
                        arManager.DropCity();
                        _tapCount = 0;
                        break;
                }
            }

            if (Time.time > _newTime)
                _tapCount = 0;
        }
    }
}