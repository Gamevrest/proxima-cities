using System;

[Serializable]
public class SeedBasic : Seed
{
    public SeedBasic() : base(SeedType.BASIC, 3)
    {
        addReward(new SeedGoldReward(100));
        addReward(new SeedGoldReward(20, true));
    }
}