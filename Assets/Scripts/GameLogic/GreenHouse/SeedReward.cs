using System;

[Serializable]
public enum SeedRewardType // Il manque la reward pour avoir de nouvelles graines ??
{
    GOLD,
    GREENHOUSE_EXP,
    REDUCE_POLUTION,
    RANDOM_BOOSTED
}

[Serializable]
public class SeedReward
{
    private SeedRewardType _type;
    private bool _boost;

    protected SeedReward(SeedRewardType type, bool boost = false)
    {
        _type = type;
        _boost = boost;
    }

    public SeedRewardType GetSeedRewardType()
    {
        return _type;
    }

    public bool IsBoosted()
    {
        return _boost;
    }
}