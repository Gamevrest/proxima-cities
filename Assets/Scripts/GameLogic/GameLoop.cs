using System.Collections.Generic;
using Tools;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

public class GameLoop : MonoBehaviour
{
    public GameManager gameManager;
    public EventManager eventManager;
    [ReadOnly] public List<string> loadedScenes = new List<string>();
    public List<SceneReference> scenesToLoad;
    public GameObject loadingCanvas;
    private bool _unload;
    public bool skipMissions; // Booleen pou skipper le tuto
    public bool isDemo;
    public GameObject devConsolePrefab;

    private void Awake()
    {
       // if (!FindObjectOfType<DevConsole>()) Instantiate(devConsolePrefab);
        isDemo = false; //PlayerPrefs.GetInt("DEMO") == 1;
        eventManager = EventManager.Instance;
        gameManager = GameManager.Instance;
        eventManager.SubscribeAll(evt =>
        {
            if (evt.getEventType() != GameEventType.GAME_LOOP)
                Debug.Log("MAINDEBUG : " + evt.getEventType());
        });
        eventManager.Subscribe(Error);

        SceneManager.sceneLoaded += SceneManagerOnSceneLoaded;
        SceneManager.sceneUnloaded += SceneManagerOnSceneUnloaded;
        eventManager.Subscribe(OnStartLoading);
        eventManager.Subscribe(OnStopLoading);
        gameManager.LoadData();
        eventManager.Raise(new StartLoadingEvent());
    }

    public void LoadScenes()
    {
        foreach (var scene in scenesToLoad)
            if (!loadedScenes.Contains(scene))
                SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
    }

    public void UnloadScenes()
    {
        _unload = true;
        EventManager.Instance.Raise(new StartLoadingEvent());
        foreach (var scene in scenesToLoad)
            if (loadedScenes.Contains(scene))
                SceneManager.UnloadSceneAsync(scene);
    }

    private void SceneManagerOnSceneUnloaded(Scene arg0) => loadedScenes.Remove(arg0.name);

    private void StartFirstMissionTuto()
    {
        DevConsole.Log($"demo status : {isDemo}");
        MissionHandler.Action(new PushMissionMissionAction(MissionName.TUTO_HOUSE));
    }

    private void SceneManagerOnSceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        loadedScenes.Add(arg0.name);
        if (loadedScenes.Count != scenesToLoad.Count || gameManager.GetGameExist()) return;
        if (!Debug.isDebugBuild || !skipMissions)
            Invoke(nameof(StartFirstMissionTuto), 1);
        gameManager.SetGameExist();
    }

    private void Start()
    {
        LoadScenes();
        InvokeRepeating(nameof(OneSecondLoop), 0f, 1f);
    }

    private void OnApplicationQuit()
    {
        gameManager.SaveData();
    }

    public void OnDestroy()
    {
        gameManager.SaveData();
    }

    private void OneSecondLoop()
    {
        if (_unload && loadedScenes.Count == 1)
            SceneManager.LoadScene("Menus");

        gameManager.LoopGame();
        eventManager.Raise(new GameLoopEvent());
    }

    private void Error(GameErrorEvent gameEvent)
    {
        print(gameEvent.GetTitleId() + " --- " + gameEvent.GetMessageId());
    }

    private void OnStartLoading(StartLoadingEvent _)
    {
        loadingCanvas.SetActive(true);
    }

    private void OnStopLoading(StopLoadingEvent _)
    {
        loadingCanvas.SetActive(false);
    }


    void OnApplicationPause(bool pauseStatus)
    {
        gameManager.SaveData();
    }
}