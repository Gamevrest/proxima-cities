using System;

[Serializable]
public class TotalMoneyMissionRequirement : MissionRequirement
{
    public TotalMoneyMissionRequirement(long moneyCount) :base(MissionRequirementType.TOTAL_MONEY, moneyCount)
    {
    }
}