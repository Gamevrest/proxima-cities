using System;

[Serializable]
public class PollutionMissionRequirements : MissionRequirement
{
    public PollutionMissionRequirements(long pollutionCount) : base(MissionRequirementType.POLLUTION, pollutionCount)
    {
    }
}