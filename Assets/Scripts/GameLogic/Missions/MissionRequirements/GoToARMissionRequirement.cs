using System;

[Serializable]
public class GoToARMissionRequirement : MissionRequirement
{
    public GoToARMissionRequirement() : base(MissionRequirementType.GO_TO_AR)
    {
    }
}