using System;

[Serializable]
public class MissionDoneCountMissionRequirement : MissionRequirement
{

    public MissionDoneCountMissionRequirement(int count) : base(MissionRequirementType.MISSION_DONE_COUNT, count)
    {
    }

}