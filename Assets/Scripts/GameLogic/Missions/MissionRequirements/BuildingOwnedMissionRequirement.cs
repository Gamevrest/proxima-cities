using System;

[Serializable]
public class BuildingOwnedMissionRequirement : MissionRequirement
{
    private readonly BuildingType _buildingType;
    public BuildingOwnedMissionRequirement(BuildingType buildingType, int buildingCount) : base(MissionRequirementType.BUILDING_OWNED, buildingCount)
    {
        _buildingType = buildingType;
    }

    public BuildingType getBuildingType()
    {
        return _buildingType;
    }
}