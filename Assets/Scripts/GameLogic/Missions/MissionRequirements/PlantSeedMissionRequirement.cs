using System;

[Serializable]
public class PlantSeedMissionRequirement : MissionRequirement
{
    public PlantSeedMissionRequirement(int seedCount) : base(MissionRequirementType.PLANT_SEED, seedCount)
    {
    }

}