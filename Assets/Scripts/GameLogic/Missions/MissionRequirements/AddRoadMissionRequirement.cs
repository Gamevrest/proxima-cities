using System;

[Serializable]
public class AddRoadMissionRequirement : MissionRequirement
{
    public AddRoadMissionRequirement(int roadCount) : base(MissionRequirementType.ADD_ROAD, roadCount)
    {
    }
}