using System;

public enum MissionActionType
{
    SHOW_TEXT,
    SHOW_IMG,
    SHOW_PROXIMIAM,
    PUSH_MISSION,
    GIVE_GOLD,
    GIVE_SEED,
    REDUCE_POLLUTION,
    FINISH_TUTO,
    ADD_BUILDING,
}

[Serializable]
public class MissionAction
{
    private MissionActionType _type;

    protected MissionAction(MissionActionType type)
    {
        _type = type;
    }

    public MissionActionType getMissionActionType()
    {
        return _type;
    }
}