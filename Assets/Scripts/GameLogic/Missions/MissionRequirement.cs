using System;

public enum MissionRequirementType
{
    EARN_MONEY,
    TOTAL_MONEY,
    POLLUTION,
    POPULATION,
    BUILDING_OWNED,
    ADD_BUILDING,
    GROW_PLANT,
    MISSION_DONE_COUNT,
    GO_TO_AR,
    GO_TO_GREENHOUSE,
    GO_TO_2D,
    PLANT_SEED,
    ADD_ROAD,
}

[Serializable]
public class MissionRequirement
{
    private readonly MissionRequirementType _type;
    private readonly long _neededCount;
    private long _currentCount;
    private bool _isCompleted;

    protected MissionRequirement(MissionRequirementType type, long neededCount = 0)
    {
        _type = type;
        _neededCount = neededCount;
    }

    public MissionRequirementType getMissionRequirementType()
    {
        return _type;
    }

    public long getNeededCount()
    {
        return _neededCount;
    }

    public long getCurrentCount()
    {
        return _currentCount;
    }

    public void setCurrentCount(long newCurrentCount)
    {
        _currentCount = newCurrentCount;
    }
    
    public void addToCurrentCount(long amount)
    {
        if (_currentCount + amount > _neededCount)
        {
            _currentCount = _neededCount;
        }
        else
        {
            _currentCount += amount;
            
        }
    }

    public void setCompleted()
    {
        _isCompleted = true;
    }

    public bool isAchieved()
    {
        return _isCompleted || _neededCount > 0 && _currentCount >= _neededCount;
    }
}