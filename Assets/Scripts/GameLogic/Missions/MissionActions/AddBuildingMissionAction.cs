using System;
using UnityEngine;

[Serializable]
public class AddBuildingMissionAction : MissionAction
{

    private readonly BuildingType _building;
    private readonly Vector2Int _pos;
    public AddBuildingMissionAction(BuildingType newBuilding, Vector2Int newPos) : base(MissionActionType.ADD_BUILDING)
    {
        _building = newBuilding;
        _pos = newPos;
    }

    public BuildingType GetBuildingType()
    {
        return _building;
    }

    public Vector2Int GetPosition()
    {
        return _pos;
    }
}