using System;

public class BuildingTypeNotFoundException : GamevrestException
{
    public BuildingTypeNotFoundException(String type)
        : base(String.Format("Building type not found {0}", type),
            "ERROR_BUILDINGTYPE", type)
    {
    }
}