using UnityEngine;

public class EmptyPositionException : GamevrestException
{
    private Vector2 position;

    public EmptyPositionException(Vector2 newPosition)
        : base($"Empty position x:{newPosition.x} y:{newPosition.y}",
            "ERROR_EMPTYPOS", newPosition.x, newPosition.y)
    {
        position = newPosition;
    }

    public Vector2 getPosition()
    {
        return position;
    }
}