﻿using System;

public abstract class GamevrestException : Exception
{
    private readonly string _descId = "ERROR_DESC_DEFAULT";
    private readonly object[] _vars;

    protected GamevrestException(string msg) : base(msg)
    {
    }
    
    protected GamevrestException(string msg , string descId, params object[] vars) : base(msg)
    {
        _descId = descId;
        _vars = vars;
    }

    public string GetDescId()
    {
        return _descId;
    }

    public object[] GetVars()
    {
        return _vars;
    }
}