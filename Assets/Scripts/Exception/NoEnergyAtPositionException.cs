using UnityEngine;

public class NoEnergyAtPositionException : GamevrestException
{
    private Vector2Int position;
    public NoEnergyAtPositionException(Vector2Int newPosition)
        : base($"No Energy at position x:{newPosition.x} y:{newPosition.y}",
            "ERROR_NOENERGY", newPosition.x, newPosition.y)
    {
        position = newPosition;
    }

    public Vector2Int getPosition()
    {
        return position;
    }
}