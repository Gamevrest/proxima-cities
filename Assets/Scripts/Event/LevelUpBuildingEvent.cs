using UnityEngine;

public class LevelUpBuildingEvent : GameEvent
{
    private Vector2Int position;
    public LevelUpBuildingEvent() : base(GameEventType.LEVEL_UP_BUILDING)
    {
    }

    public LevelUpBuildingEvent setPosition(Vector2Int newPosition)
    {

        position = newPosition;
        return this;
    }

    public Vector2Int getPosition()
    {
        return position;
    }
}