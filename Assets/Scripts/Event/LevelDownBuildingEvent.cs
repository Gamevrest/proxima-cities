using UnityEngine;

public class LevelDownBuildingEvent : GameEvent
{
    private Vector2Int position;

    public LevelDownBuildingEvent() : base(GameEventType.LEVEL_DOWN_BUILDING)
    {
        
    }

    public LevelDownBuildingEvent setPosition(Vector2Int newPosition)
    {
        position = newPosition;
        return this;
    }

    public Vector2Int getPosition()
    {
        return position;
    }
}