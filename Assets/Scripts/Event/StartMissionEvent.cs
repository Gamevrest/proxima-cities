public class StartMissionEvent : GameEvent
{
    private int _missionIndex;
    public StartMissionEvent() : base(GameEventType.START_MISSION)
    {   
    }
    
    public StartMissionEvent setMissionIndex(int index)
    {
        _missionIndex = index;
        return this;
    }

    public int getMissionIndex()
    {
        return _missionIndex;
    }
}