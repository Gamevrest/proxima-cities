using UnityEngine;

public class RemoveRoadEvent : GameEvent
{
    private Vector2Int _position;
    private RoadType _type;
    private int _rotation;
    public RemoveRoadEvent() : base(GameEventType.REMOVE_ROAD)
    {
    }

    public RemoveRoadEvent setPosition(Vector2Int newPos)
    {
        _position = newPos;
        return this;
    }

    
    public Vector2Int getPosition()
    {
        return _position;
    }
    
    public RemoveRoadEvent setRoadType(RoadType newType)
    {
        _type = newType;
        return this;
    }

    
    public RoadType getRoadType()
    {
        return _type;
    }
    
    public RemoveRoadEvent setRotation(int newRot)
    {
        _rotation = newRot;
        return this;
    }

    
    public int getRotation()
    {
        return _rotation;
    }
}