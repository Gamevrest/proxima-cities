public class CollectPlantEvent : GameEvent
{

    private int _potIndex = -1;
    private int _potIndex2 = -1;
    public CollectPlantEvent() : base(GameEventType.COLLECT_PLANT)
    {
        
    }

    public CollectPlantEvent setPotIndex(int potIndex)
    {
        _potIndex = potIndex;
        return this;
    }

    
    public int getPotIndex()
    {
        return _potIndex;
    }
    
    public CollectPlantEvent setSecondPotIndex(int potIndex)
    {
        _potIndex2 = potIndex;
        return this;
    }

    
    public int getSecondPotIndex()
    {
        return _potIndex2;
    }
    
}