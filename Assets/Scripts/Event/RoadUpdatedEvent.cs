using UnityEngine;

public class RoadUpdatedEvent : GameEvent
{

    private Vector2Int _position;
    private RoadType _type;
    private int _rotation;
    public RoadUpdatedEvent() : base(GameEventType.ROAD_UPDATED)
    {
        
    }
    
    public RoadUpdatedEvent setPosition(Vector2Int newPos)
    {
        _position = newPos;
        return this;
    }

    
    public Vector2Int getPosition()
    {
        return _position;
    }
    
    public RoadUpdatedEvent setRoadType(RoadType newType)
    {
        _type = newType;
        return this;
    }

    
    public RoadType getRoadType()
    {
        return _type;
    }
    
    public RoadUpdatedEvent setRotation(int newRot)
    {
        _rotation = newRot;
        return this;
    }

    
    public int getRotation()
    {
        return _rotation;
    }
}