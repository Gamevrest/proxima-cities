public class MissionCollectedEvent : GameEvent
{
    private int _missionIndex;
    public MissionCollectedEvent() : base(GameEventType.MISSION_COLLECTED)
    {
        
    }
    
    public MissionCollectedEvent setMissionIndex(int index)
    {
        _missionIndex = index;
        return this;
    }

    public int getMissionIndex()
    {
        return _missionIndex;
    }
}