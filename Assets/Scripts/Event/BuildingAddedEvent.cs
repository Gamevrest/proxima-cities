using UnityEngine;

public class BuildingAddedEvent : GameEvent
{
    
    private Vector2 position;
    private BuildingType type;
    private bool _force;

    public BuildingAddedEvent() : base(GameEventType.BUILDING_ADDED)
    {
                
    }
    
    public BuildingAddedEvent setPosition(Vector2 newPosition)
    {
        position = newPosition;
        return this;
    }
    
    public BuildingAddedEvent setForce(bool newForce)
    {
        _force = newForce;
        return this;
    }

    public bool getForce()
    {
        return _force;
    }

    public Vector2 getPosition()
    {
        return position;
    }

    public BuildingAddedEvent setBuildingType(BuildingType newType)
    {
        type = newType;
        return this;
    }

    public BuildingType getBuildingType()
    {
        return type;
    }
}