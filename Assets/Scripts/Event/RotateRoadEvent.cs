using UnityEngine;

public class RotateRoadEvent : GameEvent
{
    private Vector2Int _position;
    private int _rotation;

    public RotateRoadEvent() : base(GameEventType.ROTATE_ROAD)
    {
    }

    public RotateRoadEvent setPosition(Vector2Int newPos)
    {
        _position = newPos;
        return this;
    }


    public Vector2Int getPosition()
    {
        return _position;
    }

    public RotateRoadEvent setRotation(int newRot)
    {
        _rotation = newRot;
        return this;
    }


    public int getRotation()
    {
        return _rotation;
    }
}