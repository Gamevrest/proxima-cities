using UnityEngine;

public class BuildingRenamedEvent : GameEvent
{
    private Vector2 position;
    private string name;

    public BuildingRenamedEvent() : base(GameEventType.BUILDING_RENAMED)
    {
                
    }

    public BuildingRenamedEvent setPosition(Vector2 newPosition)
    {
        position = newPosition;
        return this;
    }

    public Vector2 getPosition()
    {
        return position;
    }

    public BuildingRenamedEvent setBuildingName(string newName)
    {
        name = newName;
        return this;
    }

    public string getBuildingName()
    {
        return name;
    }
}