using UnityEngine;

public class RenameBuildingEvent : GameEvent
{
    private Vector2Int position;
    private string name;

    public RenameBuildingEvent() : base(GameEventType.RENAME_BUILDING)
    {
                
    }

    public RenameBuildingEvent setPosition(Vector2Int newPosition)
    {
        position = newPosition;
        return this;
    }

    public Vector2Int getPosition()
    {
        return position;
    }

    public RenameBuildingEvent setBuildingName(string newName)
    {
        name = newName;
        return this;
    }

    public string getBuildingName()
    {
        return name;
    }
}