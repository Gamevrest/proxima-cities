using UnityEngine;

public class AddRoadEvent : GameEvent
{
    private int _neighbours;
    private Vector2Int _position;
    private int _rotation;
    private RoadType _type;

    public AddRoadEvent() : base(GameEventType.ADD_ROAD)
    {
    }

    public AddRoadEvent setPosition(Vector2Int newPos)
    {
        _position = newPos;
        return this;
    }


    public Vector2Int getPosition()
    {
        return _position;
    }

    public AddRoadEvent setRoadType(RoadType newType)
    {
        _type = newType;
        return this;
    }


    public RoadType getRoadType()
    {
        return _type;
    }

    public AddRoadEvent setRotation(int newRot)
    {
        _rotation = newRot;
        return this;
    }


    public int getRotation()
    {
        return _rotation;
    }

    public AddRoadEvent setNeighbours(int neighbours)
    {
        _neighbours = neighbours;
        return this;
    }


    public int getNeighbours()
    {
        return _neighbours;
    }
}