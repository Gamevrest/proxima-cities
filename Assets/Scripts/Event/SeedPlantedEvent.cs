public class SeedPlantedEvent : GameEvent
{

    private int _potIndex;
    private SeedType _type;
    public SeedPlantedEvent() : base(GameEventType.SEED_PLANTED)
    {
        
    }

    public SeedPlantedEvent setPotIndex(int potIndex)
    {
        _potIndex = potIndex;
        return this;
    }

    
    public int getPotIndex()
    {
        return _potIndex;
    }
    
    public SeedPlantedEvent setSeedType(SeedType type)
    {
        _type = type;
        return this;
    }

    
    public SeedType getSeedType()
    {
        return _type;
    }
    
}