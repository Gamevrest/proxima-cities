using UnityEngine;

public class RoadAddedEvent : GameEvent
{
    
    private Vector2Int _position;
    private RoadType _type;
    private int _rotation;
    public RoadAddedEvent() : base(GameEventType.ROAD_ADDED)
    {
        
    }

    public RoadAddedEvent setPosition(Vector2Int newPos)
    {
        _position = newPos;
        return this;
    }

    
    public Vector2Int getPosition()
    {
        return _position;
    }
    
    public RoadAddedEvent setRoadType(RoadType newType)
    {
        _type = newType;
        return this;
    }

    
    public RoadType getRoadType()
    {
        return _type;
    }
    
    public RoadAddedEvent setRotation(int newRot)
    {
        _rotation = newRot;
        return this;
    }

    
    public int getRotation()
    {
        return _rotation;
    }
}