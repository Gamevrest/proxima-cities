using System;

public class GameErrorEvent : GameEvent
{
    private string _titleId = "ERROR_TITLE_DEFAULT";
    private string _descId = "ERROR_DESC_DEFAULT";
    private object[] _vars;
    private Exception _exception;

    public GameErrorEvent() : base(GameEventType.GAME_ERROR)
    {
    }

    public GameErrorEvent SetTitleId(string newId)
    {
        _titleId = newId;
        return this;
    }

    public string GetTitleId()
    {
        return _titleId;
    }

    public GameErrorEvent SetMessageId(string newMessage)
    {
        _descId = newMessage;
        return this;
    }

    public string GetMessageId()
    {
        return _descId;
    }

    public GameErrorEvent SetVars(params object[] vars)
    {
        _vars = vars;
        return this;
    }

    public object[] GetVars()
    {
        return _vars;
    }

    public GameErrorEvent SetException(Exception newException)
    {
        _exception = newException;
        _vars = new object[] {newException.Message};
        return this;
    }

    public GameErrorEvent SetException(GamevrestException newException)
    {
        _exception = newException;
        _descId = newException.GetDescId();
        _vars = newException.GetVars();
        return this;
    }

    public Exception GetException()
    {
        return _exception;
    }
}