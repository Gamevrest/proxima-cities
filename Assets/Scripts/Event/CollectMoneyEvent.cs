using UnityEngine;

public class CollectMoneyEvent : GameEvent
{
    private Vector2Int position;

    public CollectMoneyEvent() : base(GameEventType.COLLECT_MONEY)
    {
                
    }

    public CollectMoneyEvent setPosition(Vector2Int newPosition)
    {
        position = newPosition;
        return this;
    }

    public Vector2Int getPosition()
    {
        return position;
    }
}