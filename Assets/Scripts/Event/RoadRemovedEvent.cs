using UnityEngine;

public class RoadRemovedEvent : GameEvent
{

    private Vector2Int _position;
    private RoadType _type;
    private int _rotation;
    public RoadRemovedEvent() : base(GameEventType.ROAD_REMOVED)
    {
    }
    
    public RoadRemovedEvent setPosition(Vector2Int newPos)
    {
        _position = newPos;
        return this;
    }

    
    public Vector2Int getPosition()
    {
        return _position;
    }
    
    public RoadRemovedEvent setRoadType(RoadType newType)
    {
        _type = newType;
        return this;
    }

    
    public RoadType getRoadType()
    {
        return _type;
    }
    
    public RoadRemovedEvent setRotation(int newRot)
    {
        _rotation = newRot;
        return this;
    }

    
    public int getRotation()
    {
        return _rotation;
    }
}