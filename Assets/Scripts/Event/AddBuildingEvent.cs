using UnityEngine;

public class AddBuildingEvent : GameEvent
{
    private Vector2Int position;
    private BuildingType type;

    public AddBuildingEvent() : base(GameEventType.ADD_BUILDING)
    {
                
    }

    public AddBuildingEvent setPosition(Vector2Int newPosition)
    {
        position = newPosition;
        return this;
    }

    public Vector2Int getPosition()
    {
        return position;
    }

    public AddBuildingEvent setBuildingType(BuildingType newType)
    {
        type = newType;
        return this;
    }

    public BuildingType getBuildingType()
    {
        return type;
    }
}