﻿using Translation;

namespace Event
{
    public class ChangeLanguageEvent : GameEvent
    {
        private Tr.Languages _lng;

        public ChangeLanguageEvent(Tr.Languages lng = Tr.Languages.anglais) : base(GameEventType.CHANGE_LANGUAGE)
        {
            _lng = lng;
        }

        public ChangeLanguageEvent SetLanguage(Tr.Languages lng)
        {
            _lng = lng;
            return this;
        }

        public Tr.Languages GetLanguage() => _lng;
    }
}