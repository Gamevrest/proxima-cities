﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WaterTurbine : BuildingsAbstract
{
    public WaterTurbine() : base (BuildingType.WATER_TURBINE, BuildingClickAction.OPEN_INFO)
    {
        setPrice(18000);
        setPollutionBase(40);
        setEnergyGenerateBase(5);
        setEnergyZone(new List<Vector2Int>
        {
            new Vector2Int(-2,-2), new Vector2Int(-1,-2), new Vector2Int(0,-2), new Vector2Int(1,-2), new Vector2Int(2,-2),
                                         new Vector2Int(-1,-1), new Vector2Int(0,-1), new Vector2Int(1,-1),
                                         new Vector2Int(-1,0), new Vector2Int(0,0), new Vector2Int(1,0),
                                         new Vector2Int(-1,1), new Vector2Int(0,1), new Vector2Int(1,1),
            new Vector2Int(-2,2), new Vector2Int(-1,2), new Vector2Int(0,2), new Vector2Int(1,2), new Vector2Int(2,2),
        });
    }
}