using System;

namespace Buildings
{
    [Serializable]
    public class BuildingData
    {
        public BuildingType BuildingType = BuildingType.EMPTY;

        public int BuildingLevel = 1;

        public long CitizensBase;
        public long EnergyGenerateBase;
        public long EnergyNeededBase;
        public long MoneyPerMinuteBase;
        public long PolutionBase;

        public long MoneyToCollect;
        
        public string Name;

        public int PositionX;
        public int PositionY;
    }
}