using System;

[Serializable]
public class LargeEconomicalHabitation : BuildingsAbstract
{
    
    public LargeEconomicalHabitation() : base (BuildingType.LARGE_ECONOMICAL_HABITATION, BuildingClickAction.COLLECT_MONEY)
    {
        setPrice(21000);
        setMaxMoneyToCollect(16000);
        setCitizenBase(6);
        setEnergyNeededBase(2);
        setMoneyPerMinuteBase(200);
        setPollutionBase(60);
    }
}