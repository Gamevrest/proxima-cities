using System;
using System.Collections.Generic;
using Buildings;
using UnityEngine;

[Serializable]
public abstract class BuildingsAbstract
{
    private BuildingData _buildingData;
    private BuildingType _buildingType
    {
        get => _buildingData.BuildingType;
        set => _buildingData.BuildingType = value;
    }

    private int _buildingLevel
    {
        get => _buildingData.BuildingLevel;
        set => _buildingData.BuildingLevel = value;
    }

    private long _citizensBase
    {
        get => _buildingData.CitizensBase;
        set => _buildingData.CitizensBase = value;
    }

    private long _energyGenerateBase
    {
        get => _buildingData.EnergyGenerateBase;
        set => _buildingData.EnergyGenerateBase = value;
    }

    private long _energyNeededBase
    {
        get => _buildingData.EnergyNeededBase;
        set => _buildingData.EnergyNeededBase = value;
    }

    private long _moneyPerMinuteBase
    {
        get => _buildingData.MoneyPerMinuteBase;
        set => _buildingData.MoneyPerMinuteBase = value;
    }

    private long _polutionBase
    {
        get => _buildingData.PolutionBase;
        set => _buildingData.PolutionBase = value;
    }

    private long _moneyToCollect
    {
        get => _buildingData.MoneyToCollect;
        set => _buildingData.MoneyToCollect = value;
    }

    private string _name
    {
        get => _buildingData.Name;
        set => _buildingData.Name = value;
    }

    private int _positionX
    {
        get => _buildingData.PositionX;
        set => _buildingData.PositionX = value;
    }

    private int _positionY
    {
        get => _buildingData.PositionY;
        set => _buildingData.PositionY = value;
    }
    
    private readonly long _citizensFactor = 1;
    private readonly double _energyGenerateFactor = 1;
    private readonly double _energyNeededFactor = 1;
    private readonly double _moneyPerMinuteFactor = 1;
    private readonly double _pollutionFactor = 1;

    private List<BuildingType> _upgradeList = new List<BuildingType>();
    private List<Vector2Int> _energyZone = new List<Vector2Int>();
    
    private long _maxMoneyToCollect;
    private long _price;

    private readonly BuildingClickAction _clickAction;
    
    protected BuildingsAbstract(BuildingType buildingType, BuildingClickAction clickAction)
    {
        _buildingData = new BuildingData();
        _buildingType = buildingType;
        _clickAction = clickAction;
        _name = buildingType.getDefaultName();
    }

    public BuildingClickAction getClickAction()
    {
        return _clickAction;
    }

    public BuildingData getBuildingData()
    {
        return _buildingData;
    }

    public void setBuildingData(BuildingData data)
    {
        _buildingData = data;
    }

    public void setName(string newName)
    {
        _name = newName;
    }

    public string getName()
    {
        return _name;
    }

    public BuildingType getBuildingType()
    {
        return _buildingType;
    }

    public void setPosition(Vector2Int newPosition)
    {
        _positionX = newPosition.x;
        _positionY = newPosition.y;
    }

    public Vector2Int getPosition()
    {
        return new Vector2Int(_positionX, _positionY);
    }

    public long getPollution()
    {
        return Convert.ToInt64(_polutionBase * (_buildingLevel * _pollutionFactor));
    }

    public long getCitizens()
    {
        return _citizensBase * _buildingLevel * _citizensFactor;
    }

    public long getMoneyPerMinute()
    {
        return Convert.ToInt64(_moneyPerMinuteBase * (_buildingLevel * _moneyPerMinuteFactor));
    }

    public long getEnergyGenerate()
    {
        return Convert.ToInt64(_energyGenerateBase * (_buildingLevel * _energyGenerateFactor));
    }

    public long getEnergyNeeded()
    {
        return Convert.ToInt64(_energyNeededBase * (_buildingLevel * _energyNeededFactor));
    }

    public int getBuildingLevel()
    {
        return _buildingLevel;
    }

    public long getPrice()
    {
        return _price;
    }

    public void levelUp()
    {
        _buildingLevel++;
    }

    public void levelDown()
    {
        _buildingLevel--;
    }

    public List<BuildingType> getAvailableUpdagrade()
    {
        return _upgradeList;
    }

    public List<Vector2Int> getEnergyZone()
    {
        return _energyZone;
    }
    
    public void setEnergyZone(List<Vector2Int> zone)
    {
        _energyZone = zone;
    }

    protected void setPollutionBase(long amount)
    {
        _polutionBase = amount;
    }

    protected void setCitizenBase(long amount)
    {
        _citizensBase = amount;
    }

    protected void setEnergyGenerateBase(long amount)
    {
        _energyGenerateBase = amount;
    }

    protected void setEnergyNeededBase(long amount)
    {
        _energyNeededBase = amount;
    }

    protected void setMoneyPerMinuteBase(long amount)
    {
        _moneyPerMinuteBase = amount;
    }

    protected void setPrice(long amount)
    {
        _price = amount;
    }

    protected void setUpgradeList(List<BuildingType> newUpgradeList)
    {
        _upgradeList = newUpgradeList;
    }

    protected void addUpgradeToList(BuildingType newUpgrade)
    {
        _upgradeList.Add(newUpgrade);
    }

    protected void setMaxMoneyToCollect(long newMaxMoneyToCollect)
    {
        _maxMoneyToCollect = newMaxMoneyToCollect;
    }

    public long getMaxMoneyToCollect()
    {
        return _maxMoneyToCollect;
    }

    public void setMoneyToCollect(long newMoneyToCollect)
    {
        _moneyToCollect = newMoneyToCollect;
    }

    public long getMoneyToCollect()
    {
        return Convert.ToInt64(_moneyToCollect);
    }

    public long collectMoney()
    {
        var returnMoney = Convert.ToInt64(_moneyToCollect);
        _moneyToCollect -= returnMoney;
        return returnMoney;
    }

    public void addMoneyToCollect(float productionPercentage, bool perSecond = true)
    {
        if (perSecond)
            _moneyToCollect += Convert.ToInt64(getMoneyPerMinute() * (productionPercentage/100) / 60);
        else
            _moneyToCollect += Convert.ToInt64(getMoneyPerMinute() * (productionPercentage/100));
        if (_maxMoneyToCollect != 0 && _moneyToCollect >= _maxMoneyToCollect) _moneyToCollect = _maxMoneyToCollect;
    }
    
    public void addOfflineGain(float productionPercentage, double minutes)
    {
        _moneyToCollect += Convert.ToInt64(getMoneyPerMinute() * productionPercentage * minutes);
        if (_maxMoneyToCollect != 0 && _moneyToCollect >= _maxMoneyToCollect) _moneyToCollect = _maxMoneyToCollect;
    }

    public long GetMoneyGeneratedOverHour(float productionPercentage)
    {
        return Convert.ToInt64(getMoneyPerMinute() * productionPercentage * 60);
    }

    public override string ToString()
    {
        var msg = "";
        msg += "pollution :" + _polutionBase + "\n";
        msg += "energi need :" + _energyNeededBase + "\n";
        msg += "energi gene :" + _energyGenerateBase + "\n";
        msg += "price :" + _price + "\n";
        msg += "defautl name :" + _buildingType.getDefaultName() + "\n";
        return msg;
    }
}