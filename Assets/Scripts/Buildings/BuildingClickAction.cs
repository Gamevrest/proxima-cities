public enum BuildingClickAction
{
    COLLECT_MONEY,
    OPEN_INFO,
    OPEN_TOWNHALL,
    OPEN_GREENHOUSE
}