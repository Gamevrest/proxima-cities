using System;


[Serializable]
public class PlasticRecyclingStation : BuildingsAbstract
{
    public PlasticRecyclingStation() : base (BuildingType.PLASTIC_RECYCLING_STATION, BuildingClickAction.OPEN_INFO)
    {
        setPrice(40000);
        setEnergyNeededBase(7);
        setPollutionBase(-100);
    }
}