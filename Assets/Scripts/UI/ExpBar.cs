﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ExpBar : MonoBehaviour
{
    // Start is called before the first frame update
    public TextMeshProUGUI currLvl;

    private TownHall _townHall;
    private Slider _slider;
    
    private EventManager _eventManager = EventManager.Instance;
    
    void Start()
    {
        _townHall = GameManager.Instance.GetTownHall();
        _slider = GetComponent<Slider>();

        UpdateExpBar();
        
        _eventManager.Subscribe(XpGainedEvent);
    }


    void UpdateExpBar() {
        currLvl.text = _townHall.GetLevel().ToString();

        _slider.maxValue = _townHall.GetExperienceToNextLevel();
        _slider.value = _townHall.GetExperience();
    }

    private void XpGainedEvent(XPGainedEvent evt) {
        UpdateExpBar();
    }
}
