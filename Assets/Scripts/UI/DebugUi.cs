using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    public class DebugUi : MonoBehaviour
    {
        private void Awake()
        {
            if (!Debug.isDebugBuild)
                DestroyImmediate(gameObject);
        }
#if DEBUG
        public void Reload2D()
        {
            SceneManager.UnloadSceneAsync("2D");
            SceneManager.LoadSceneAsync("2D", LoadSceneMode.Additive);
        }

        public void SaveProfile(string n)
        {
            GameManager.Instance.SaveDataDebug(n);
        }

        public void LoadProfile(string n)
        {
            GameManager.Instance.LoadDataDebug(n);
            Reload2D();
        }

        public void GIVEMEMONEY(int amount)
        {
            GameManager.Instance.AddMoney(amount);
        }
#endif
    }
}