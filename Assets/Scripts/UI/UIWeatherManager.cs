﻿using System.Collections;
using System.Collections.Generic;
using GameLogic.Weather;
using UnityEngine;

public class UIWeatherManager : MonoBehaviour {

    public GameObject sun;
    public GameObject rain;
    public GameObject wind;
    public GameObject storm;

    private GameObject _currWeatherView;
    private WeatherState _currWeatherState;
    
    private readonly EventManager _eventManager = EventManager.Instance;
    
    void Start() {
        _currWeatherView = sun;
        _currWeatherState = GameManager.Instance.GetCurrentWeather();
        _eventManager.Subscribe(ChangeWeatherEvent);
        UpdateWeather(_currWeatherState);
    }

    private void ChangeWeatherEvent(WeatherChangeEvent evt) {
        if (_currWeatherState == GameManager.Instance.GetCurrentWeather())
            return;
        UpdateWeather(evt.GetWeather());
    }

    private void UpdateWeather(WeatherState weatherState) {
        _currWeatherView.SetActive(false);
        _currWeatherState = weatherState;
        switch (_currWeatherState) {
            case WeatherState.SUNNY:
                _currWeatherView = sun;
                break;
            case WeatherState.RAINY:
                _currWeatherView = rain;
                break;
            case WeatherState.WINDY:
                _currWeatherView = wind;
                break;
            case WeatherState.STORMY:
                _currWeatherView = storm;
                break;
            default:
                _currWeatherView = sun;
                break;
        }

        _currWeatherView.SetActive(true);
    }
}
