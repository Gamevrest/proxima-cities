﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CityName : MonoBehaviour {

    public TextMeshProUGUI currCityName;
    public TextMeshProUGUI newCityName;
    public string defaultName = "Your city name";

    private TownHall _townHall;
    
    // Start is called before the first frame update
    void Start() {
        _townHall = GameManager.Instance.GetTownHall();
        currCityName.text = String.IsNullOrEmpty(_townHall.GetCityName()) ? defaultName : _townHall.GetCityName();
    }

    public void UpdateCityName() {
        _townHall.SetCityName(newCityName.text);
        currCityName.text = newCityName.text;
    }
}
