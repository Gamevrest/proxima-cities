using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class DropMe : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
    {
        [HideInInspector] public static bool moving;
        [HideInInspector] public static BuildingsAbstract movingBuilding;
        private BuildingsAbstract building;
        private Vector2Int buildingPos;
        private BuildingType buildingType;
        public Image containerImage;
        private Sprite dropSprite;
        public EventManager eventManager = EventManager.Instance;
        [HideInInspector] public bool hasUpgrade = false;
        public Color highlightColor = Color.yellow;
        public Image highlight;
        [HideInInspector] public bool isEmpty = true;
        public Animation moneyCollectedAnim;
        public AudioSource moneyCollectSound;
        public TextMeshProUGUI moneyCollectedText;

        private Color normalColor;
        public Image receivingImage;
        private List<BuildingType> upgradeList;

        private WindowManager windowManager;

        public void OnDrop(PointerEventData data)
        {
            containerImage.color = normalColor;

            if (receivingImage == null)
                return;

            dropSprite = GetDropSprite(data);
            if (dropSprite != null)
            {
                eventManager.Subscribe(GetErrorEmpty);

                print("cellPos: " + buildingPos);

                AddBuildingEvent newBuilding = new AddBuildingEvent();
                newBuilding.setPosition(buildingPos);
                newBuilding.setBuildingType(buildingType);
                eventManager.Raise(newBuilding);
            }
        }

        public void OnPointerClick(PointerEventData pointerEventData)
        {
            if (moving)
            {
                MoveBuilding();
                windowManager.HideMovingScreen();
                moving = false;
                return;
            }
            windowManager.HideWindows();
            if (!isEmpty)
                windowManager.ShowBuildingOptions(transform.GetComponent<DropMe>());
            else
            {
                if (windowManager.IsBuildingOptionsVisible())
                    windowManager.HideBuildingOptions();
                return;
            }
            switch (building.getClickAction()) {
                case BuildingClickAction.COLLECT_MONEY:
                    CollectMoney();
                    break;
                case BuildingClickAction.OPEN_TOWNHALL:
                    windowManager.ShowBuildingInfo();
                    break;
                case BuildingClickAction.OPEN_GREENHOUSE:
                    windowManager.ShowBuildingInfo();
                    break;
                case BuildingClickAction.OPEN_INFO:
                    break;
                default:
                    return;
            }
        }

        public void OnPointerEnter(PointerEventData data)
        {
            if (moving) {
                containerImage.color = highlightColor;
                return;
            }

            if (containerImage == null)
                return;

            Sprite dropSprite = GetDropSprite(data);
            if (dropSprite != null)
                containerImage.color = highlightColor;
        }

        public void OnPointerExit(PointerEventData data)
        {
            if (moving) {
                containerImage.color = normalColor;
                return;
            }
            if (containerImage == null)
                return;

            containerImage.color = normalColor;
        }

        private void Start()
        {
            eventManager.Subscribe(AddedBuildingEvent);
            eventManager.Subscribe(UpgradedBuildingEvent);
            eventManager.Subscribe(MovedBuildingEvent);
            eventManager.Subscribe(MoneyCollected);
            eventManager.Subscribe(BuildingSold);
            windowManager = GameObject.Find("WindowManager").GetComponent<WindowManager>();
        }

        public void InitBuilding(BuildingsAbstract actualBuilding)
        {
            isEmpty = false;
            SetBuilding(actualBuilding);
            SetBuildingType(actualBuilding.getBuildingType());
            if (actualBuilding.getAvailableUpdagrade().Count > 0)
            {
                upgradeList = actualBuilding.getAvailableUpdagrade();
                hasUpgrade = true;
            }

            receivingImage.sprite = Resources.Load<Sprite>("UI/" + actualBuilding.getBuildingType());
        }

        public void RemoveBuilding()
        {
            isEmpty = true;
            building = null;
            buildingType = BuildingType.EMPTY;
            upgradeList = null;
            receivingImage.sprite = Resources.Load<Sprite>("UI/blank");
        }

        private void AddedBuildingEvent(BuildingAddedEvent evt)
        {
            if (evt.getPosition() != buildingPos) return;
            InitBuilding(GameManager.Instance.GetBuildingByPosition(buildingPos));
            if (!evt.getForce())
                eventManager.UnSubscribe(GetErrorEmpty);
        }

        private void GetErrorEmpty(GameErrorEvent evt)
        {
            var t = evt.GetException() as NotEmptyPositionException;
            if (t != null)
            {
                print(evt.GetMessageId());
                eventManager.UnSubscribe(GetErrorEmpty);
            }
        }

        public void OnEnable()
        {
            if (containerImage != null)
                normalColor = containerImage.color;
        }

        private Sprite GetDropSprite(PointerEventData data)
        {
            var originalObj = data.pointerDrag;
            if (originalObj == null)
                return null;

            var dragMe = originalObj.GetComponent<DragMe>();
            if (dragMe == null)
                return null;
            if (dragMe.isRoad)
                return null;
            buildingType = dragMe.type;
            var srcImage = originalObj.GetComponent<Image>();
            if (srcImage == null)
                return null;

            return srcImage.sprite;
        }

        public void SetBuilding(BuildingsAbstract build)
        {
            building = build;
        }

        public BuildingsAbstract GetBuilding()
        {
            return building;
        }

        public void SetPos(Vector2Int pos)
        {
            buildingPos = pos;
        }

        public Vector2 GetPos()
        {
            return buildingPos;
        }

        public void SetBuildingType(BuildingType type)
        {
            buildingType = type;
        }

        public BuildingType GetBuildingType()
        {
            return buildingType;
        }

        public void UpgradeBuilding(BuildingType type)
        {
            windowManager.HideEverything();
            UpgradeBuildingEvent upgradeBuilding = new UpgradeBuildingEvent();
            upgradeBuilding.setPosition(buildingPos).setNewBuildingType(type).setOldBuildingType(buildingType);
            eventManager.Raise(upgradeBuilding);
            Debug.Log("Building upgrade, pos: " + buildingPos + " New type: " + type + " Old type: " + buildingType);
        }

        private void UpgradedBuildingEvent(BuildingUpgradedEvent evt)
        {
            if (evt.getPosition() == buildingPos)
            {
                InitBuilding(GameManager.Instance.GetBuildingByPosition(buildingPos));
                Debug.Log("Building upgraded to: " + building.getBuildingType());
                Debug.Log("New upgrade available: " + building.getAvailableUpdagrade()[0] + " and " +
                          building.getAvailableUpdagrade()[1]);
            }
        }

        private void MoveBuilding()
        {
            MoveBuildingEvent moveBuilding = new MoveBuildingEvent();
            moveBuilding.setNewPosition(buildingPos).setOldPosition(movingBuilding.getPosition());
            eventManager.Raise(moveBuilding);
        }

        private void MovedBuildingEvent(BuildingMovedEvent evt)
        {
            if (evt.getNewPosition() == buildingPos)
            {
                InitBuilding(GameManager.Instance.GetBuildingByPosition(buildingPos));
                Debug.Log("Building moved from " + evt.getOldPosition() + " to " + evt.getNewPosition());
            }

            if (evt.getOldPosition() == buildingPos)
            {
                RemoveBuilding();
                Debug.Log("Building removed at " + evt.getOldPosition() + " cellPos verif: " + buildingPos);
            }
        }

        public void CollectMoney()
        {
            CollectMoneyEvent collectMoney = new CollectMoneyEvent();
            collectMoney.setPosition(buildingPos);
            eventManager.Raise(collectMoney);
        }

        private void MoneyCollected(MoneyCollectedEvent evt)
        {
            if (evt.getPosition() == buildingPos) {
                if (evt.getAmount() > 0) {
                    moneyCollectedText.text = "+" + evt.getAmount() + " Collected";
                    moneyCollectedAnim.Play();
                    moneyCollectSound.Play();
                    Debug.Log("Money collected at position " + evt.getPosition());
                }
            }
        }

        public void SellBuilding()
        {
            SellBuildingEvent sellbuilding = new SellBuildingEvent();
            sellbuilding.setPosition(buildingPos);
            eventManager.Raise(sellbuilding);
            windowManager.HideEverything();
        }

        private void BuildingSold(BuildingSoldEvent evt)
        {
            if (evt.getPosition() == buildingPos)
            {
                Debug.Log("Building sold at position " + evt.getPosition());
                RemoveBuilding();
            }
        }
    }
}