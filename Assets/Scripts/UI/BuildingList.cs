﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class BuildingList : MonoBehaviour
    {
        public GameObject buildingCard;

        private GameObject childObject;
        private bool currPanel;
        public Image switchButtonImage;

        void Start()
        {
            loadBuildingList();
        }

        private void loadBuildingList()
        {
            currPanel = true;
            foreach (BuildingType i in GameManager.AvailableBuildings.Keys)
            {
                childObject = Instantiate(buildingCard, transform);
                DragMe script = childObject.GetComponentInChildren<DragMe>();
                script.type = i;
                script.isRoad = false;
                Image buildingImage = childObject.GetComponentsInChildren<Image>()[1];
                buildingImage.sprite = Resources.Load<Sprite>("UI/" + i);
            }
        }

        private void loadRoadList()
        {
            currPanel = false;
            foreach (RoadType i in Enum.GetValues(typeof(RoadType)))
            {
                childObject = Instantiate(buildingCard, transform);
                DragMe script = childObject.GetComponentInChildren<DragMe>();
                script.isRoad = true;
                script.roadType = i;
                Image buildingImage = childObject.GetComponentsInChildren<Image>()[1];
                buildingImage.sprite = Resources.Load<Sprite>("UI/" + i);
            }
        }

        private void cleanList()
        {
            foreach (Transform children in transform)
            {
                Destroy(children.gameObject);
            }
        }

        public void switchPanel()
        {
            cleanList();
            if (currPanel)
            {
                loadRoadList();
                switchButtonImage.sprite = Resources.Load<Sprite>("UI/HOUSE");
            }
            else
            {
                loadBuildingList();
                switchButtonImage.sprite = Resources.Load<Sprite>("UI/CROSSROAD");
            }
        }
    }
}