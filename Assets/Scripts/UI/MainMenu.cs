using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using GameLogic;

namespace UI
{
    public class MainMenu : MonoBehaviour
    {
        public Button ctnButton;
        public GameObject loadingScreen;
        public GameObject MenuCanvas;
        private PlayableDirector director;
        
        private void Awake()
        {
            SceneManager.LoadSceneAsync("Cinematique", LoadSceneMode.Additive);

            if (SaveManager.CheckForExistingSave())
            {
                // ContinueGame();
            }
            else
                ctnButton.interactable = false;
        }

        public void NewGame()
        {
            GPGS.Instance.NewGameEvent();
            SaveManager.saveGame(new Data());

            MenuCanvas.SetActive(false);
            if (!director) director = FindObjectOfType<PlayableDirector>();
            director.Play();
            director.stopped += DirectorOnStopped;
        }

        private void DirectorOnStopped(PlayableDirector obj)
        {
            LoadScene("Core");
        }

        public void ContinueGame()
        {
            GPGS.Instance.LoadGameEvent();
            LoadScene("Core");
        }

        public void Exit()
        {
            Application.Quit();
        }

        public void LoadScene(string name)
        {
            loadingScreen.SetActive(true);
            SceneManager.LoadScene(name);
        }

        public void LoadScene(int id)
        {
            loadingScreen.SetActive(true);
            SceneManager.LoadScene(id);
        }
    }
}