﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class SkipCinematic : MonoBehaviour
{
    private PlayableDirector _director;

    private void Start()
    {
        _director = FindObjectOfType<PlayableDirector>();
    }

    private void Update()
    {
        if (Input.touchCount > 0 || Input.anyKey)
            _director.Stop();
    }
}