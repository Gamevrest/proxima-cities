﻿using System;
using System.Collections;
using Tools;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;

#endif
namespace Tools
{
    public class CustomButton : MonoBehaviour
    {
        public enum ButtonOrTrigger
        {
            TRIGGER,
            BUTTON
        }

        public enum ColorsOrImages
        {
            USE_COLORS,
            USE_IMAGES,
            USE_NONE
        }

        [HideInInspector] public bool foldSettings;
        [HideInInspector] public bool foldDebug;

        [Header("Debug values")] public bool selected = false;
        public bool highlighted = false;
        public bool isDelaying;

        [Header("basic Settings")] public float buttonDelay = .5f;
        public Image targetImage;
        public ButtonOrTrigger interactionType;
        public bool canDeselect = false;
        public ColorsOrImages type;
        public CustomButtonGroup buttonGroup;

        [Header("Colors for states")] public Color normalColor;
        public Color highlightColor;
        public Color selectedColor;

        [Header("Sprites for states")] public Sprite normalSprite;
        public Sprite highlightSprite;
        public Sprite selectedSprite;

        [Header("Callback events")] [HideInInspector]
        public UnityEvent highlight;

        [HideInInspector] public UnityEvent unHighlight;
        public UnityEvent @select;
        public UnityEvent deselect;

        public void Awake()
        {
            if (targetImage == null) targetImage = GetComponent<Image>();
            //ForceState(false, false);
            if (buttonGroup)
                buttonGroup.RegisterCustomButton(this);
        }

        public void ForceStateWithGroup(bool isSelected, bool isHighlighted)
        {
            if (interactionType == ButtonOrTrigger.TRIGGER)
            {
                if (buttonGroup) buttonGroup.SelectButton(this);
                selected = isSelected;
            }

            highlighted = isHighlighted;
            ChangeStateDisplay();
        }

        public void ForceState(bool isSelected, bool isHighlighted)
        {
            if (interactionType == ButtonOrTrigger.TRIGGER)
            {
                selected = isSelected;
            }

            highlighted = isHighlighted;
            ChangeStateDisplay();
        }

        private void ChangeColor()
        {
            if (!targetImage) return;
            if (selected)
                targetImage.color = selectedColor;
            else if (highlighted)
                targetImage.color = highlightColor;
            else
                targetImage.color = normalColor;
        }

        private void ChangeSprite()
        {
            if (!targetImage) return;
            if (selected)
                targetImage.sprite = selectedSprite;
            else if (highlighted)
                targetImage.sprite = highlightSprite;
            else
                targetImage.sprite = normalSprite;
        }

        private void ChangeStateDisplay()
        {
            switch (type)
            {
                case ColorsOrImages.USE_COLORS:
                    ChangeColor();
                    break;
                case ColorsOrImages.USE_IMAGES:
                    ChangeSprite();
                    break;
                case ColorsOrImages.USE_NONE:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void Highlight()
        {
            highlighted = true;
            ChangeStateDisplay();
            highlight.Invoke();
        }

        public void UnHighlight()
        {
            highlighted = false;
            ChangeStateDisplay();
            unHighlight.Invoke();
        }

        public void Select()
        {
            if (interactionType == ButtonOrTrigger.TRIGGER && selected && canDeselect)
            {
                Deselect();
                return;
            }

            if (!CanAct()) return;
            if (buttonGroup) buttonGroup.SelectButton(this);
            if (interactionType == ButtonOrTrigger.TRIGGER)
            {
                selected = true;
            }

            ChangeStateDisplay();
            @select.Invoke();
        }

        public void Deselect()
        {
            if (!CanAct()) return;
            selected = false;
            ChangeStateDisplay();
            deselect.Invoke();
        }

        private bool CanAct()
        {
            if (isDelaying) return false;
            if (buttonDelay <= 0) return true;
            StartCoroutine(WaitForDelay());
            return true;
        }

        private IEnumerator WaitForDelay()
        {
            isDelaying = true;
            yield return new WaitForSeconds(buttonDelay);
            isDelaying = false;
        }
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(CustomButton))]
public class TabletButtonEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var targ = target as CustomButton;
        if (targ == null) return;
        EditorGUILayout.Separator();
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Highlight")) targ.Highlight();
        if (GUILayout.Button("UnHighlight")) targ.UnHighlight();
        if (GUILayout.Button("Select")) targ.Select();
        if (GUILayout.Button("Deselect")) targ.Deselect();
        GUILayout.EndHorizontal();
        EditorGUILayout.Separator();
        targ.foldSettings = EditorGUILayout.Foldout(targ.foldSettings, "Settings");
        if (targ.foldSettings)
        {
            EditorGUI.indentLevel++;
            // DECOMMENTER DEFAULT ET COMMENTER CUSTOM SI IL Y A DES PROBLEMES
            //DrawDefaultInspector();
            DrawCustomInspector();
        }
    }

    public void DrawCustomInspector()
    {
        var targ = target as CustomButton;
        if (targ == null) return;
        targ.foldDebug = EditorGUILayout.Foldout(targ.foldDebug, "Debug (ReadOnly)");
        if (targ.foldDebug)
        {
            targ.selected = EditorGUILayout.Toggle("Is Selected ?", targ.selected);
            targ.highlighted = EditorGUILayout.Toggle("Is Highlighted ?", targ.highlighted);
            targ.isDelaying = EditorGUILayout.Toggle("Is on cooldown ?", targ.isDelaying);
        }

        EditorGUILayout.LabelField("General settings", EditorStyles.boldLabel);
        EditorGUI.indentLevel++;
        targ.buttonDelay = EditorGUILayout.FloatField("Button cooldown", targ.buttonDelay);
        targ.targetImage =
            (Image) EditorGUILayout.ObjectField("Target Image", targ.targetImage, typeof(Image), true);
        targ.interactionType =
            (CustomButton.ButtonOrTrigger) EditorGUILayout.EnumPopup("Interaction type", targ.interactionType);
        targ.canDeselect = EditorGUILayout.Toggle("Can be deselected", targ.canDeselect);
        targ.buttonGroup =
            (CustomButtonGroup) EditorGUILayout.ObjectField("Button Group", targ.buttonGroup,
                typeof(CustomButtonGroup),
                true);
        EditorGUI.indentLevel--;
        EditorGUILayout.LabelField("Highlight settings", EditorStyles.boldLabel);
        EditorGUI.indentLevel++;
        targ.type =
            (CustomButton.ColorsOrImages) EditorGUILayout.EnumPopup("Graphic type", targ.type);
        switch (targ.type)
        {
            case CustomButton.ColorsOrImages.USE_COLORS:
                targ.normalColor = EditorGUILayout.ColorField("Normal Color", targ.normalColor);
                targ.highlightColor = EditorGUILayout.ColorField("Highlighted Color", targ.highlightColor);
                targ.selectedColor = EditorGUILayout.ColorField("Selected Color", targ.selectedColor);
                break;
            case CustomButton.ColorsOrImages.USE_IMAGES:
                targ.normalSprite =
                    (Sprite) EditorGUILayout.ObjectField("Normal Sprite", targ.normalSprite, typeof(Sprite), false,
                        GUILayout.Height(16));
                targ.highlightSprite = (Sprite) EditorGUILayout.ObjectField("Highlighted Sprite",
                    targ.highlightSprite,
                    typeof(Sprite), false, GUILayout.Height(16));
                targ.selectedSprite = (Sprite) EditorGUILayout.ObjectField("Selected Sprite", targ.selectedSprite,
                    typeof(Sprite), false, GUILayout.Height(16));
                break;
            case CustomButton.ColorsOrImages.USE_NONE:
                break;
        }

        EditorGUI.indentLevel--;
        EditorGUILayout.LabelField("Callback settings", EditorStyles.boldLabel);
        var prop = serializedObject.FindProperty("onSelect");
        EditorGUILayout.PropertyField(prop, true);
        if (targ.canDeselect && targ.interactionType == CustomButton.ButtonOrTrigger.TRIGGER)
        {
            prop = serializedObject.FindProperty("onDeselect");
            EditorGUILayout.PropertyField(prop, true);
        }

        serializedObject.ApplyModifiedProperties();
    }
}
#endif