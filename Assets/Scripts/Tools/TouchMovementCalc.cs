﻿using System;
using UnityEngine;

namespace Tools
{
    [Serializable]
    public class TouchMovementCalc
    {
        private const float PinchTurnRatio = Mathf.PI / 2;
        private const float MinTurnAngle = 1;

        private const float PinchRatio = 1;
        private const float MinPinchDistance = 1;

        private const float PanRatio = 1;
        private const float MinPanDistance = 0;

        public float turnAngleDelta;
        public float turnAngle;

        public float pinchDistanceDelta;
        public float pinchDistance;

        public Vector2 panVectorDelta;

        public void Calculate()
        {
            pinchDistance = pinchDistanceDelta = 0;
            turnAngle = turnAngleDelta = 0;
            // if two fingers are touching the screen at the same time ...
            if (Input.touchCount != 2) return;
            var touch1 = Input.GetTouch(0);
            var touch2 = Input.GetTouch(1);
            var prevPos1 = touch1.position - touch1.deltaPosition;
            var prevPos2 = touch2.position - touch2.deltaPosition;

            // ... if at least one of them moved ...
            if (touch1.phase == TouchPhase.Moved || touch2.phase == TouchPhase.Moved)
            {
                // ... check the delta distance between them ...
                pinchDistance = Vector2.Distance(touch1.position, touch2.position);
                var prevDistance = Vector2.Distance(prevPos1, prevPos2);
                pinchDistanceDelta = pinchDistance - prevDistance;
                // ... if it's greater than a minimum threshold, it's a pinch!
                if (Mathf.Abs(pinchDistanceDelta) > MinPinchDistance)
                    pinchDistanceDelta *= PinchRatio;
                else
                    pinchDistance = pinchDistanceDelta = 0;

                // ... or check the delta angle between them ...
                turnAngle = Angle(touch1.position, touch2.position);
                var prevTurn = Angle(prevPos1, prevPos2);
                turnAngleDelta = Mathf.DeltaAngle(prevTurn, turnAngle);
                // ... if it's greater than a minimum threshold, it's a turn!
                if (Mathf.Abs(turnAngleDelta) > MinTurnAngle)
                    turnAngleDelta *= PinchTurnRatio;
                else
                    turnAngle = turnAngleDelta = 0;

                var mid = Midpoint(touch1.position, touch2.position);
                var prevMid = Midpoint(prevPos1, prevPos2);
                panVectorDelta = mid - prevMid;
            }
        }

        private static float Angle(Vector2 pos1, Vector2 pos2)
        {
            var from = pos2 - pos1;
            var to = new Vector2(1, 0);
            var result = Vector2.Angle(from, to);
            var cross = Vector3.Cross(from, to);
            if (cross.z > 0)
                result = 360f - result;
            return result;
        }

        private static Vector2 Midpoint(Vector2 pos1, Vector2 pos2)
        {
            return (pos1 + pos2) / 2f;
        }
    }
}