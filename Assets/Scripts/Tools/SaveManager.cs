using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SaveManager
{
    public static string saveName = "proxima_cities.dat";
    
    public static bool CheckForExistingSave(string sn = "")
    {
        if (sn == "") sn = saveName;
        return File.Exists(Application.persistentDataPath + "/" + sn);
    }

    public static byte[] GenerateCloudSave(Data data)
    {
        MemoryStream memoryStream = new MemoryStream();
        BinaryFormatter bf = new BinaryFormatter();
        SurrogateSelector surrogateSelector = new SurrogateSelector();
        Vector3SerializationSurrogate vector3Ss = new Vector3SerializationSurrogate();
        Vector2SerializationSurrogate vector2Ss = new Vector2SerializationSurrogate();
        Vector2IntSerializationSurrogate vector2ISs = new Vector2IntSerializationSurrogate();
        surrogateSelector.AddSurrogate(typeof(Vector3),new StreamingContext(StreamingContextStates.All),vector3Ss);
        surrogateSelector.AddSurrogate(typeof(Vector2),new StreamingContext(StreamingContextStates.All),vector2Ss);
        surrogateSelector.AddSurrogate(typeof(Vector2Int),new StreamingContext(StreamingContextStates.All),vector2ISs);
        bf.SurrogateSelector = surrogateSelector;
        bf.Serialize (memoryStream, data);
        return memoryStream.ToArray();
    }
    
    public static Data GenerateSaveFromCloudSave(byte[] bin)
    {
        MemoryStream memoryStream = new MemoryStream(bin);
        BinaryFormatter bf = new BinaryFormatter();
        SurrogateSelector surrogateSelector = new SurrogateSelector();
        Vector3SerializationSurrogate vector3Ss = new Vector3SerializationSurrogate();
        Vector2SerializationSurrogate vector2Ss = new Vector2SerializationSurrogate();
        Vector2IntSerializationSurrogate vector2ISs = new Vector2IntSerializationSurrogate();
        surrogateSelector.AddSurrogate(typeof(Vector3),new StreamingContext(StreamingContextStates.All),vector3Ss);
        surrogateSelector.AddSurrogate(typeof(Vector2),new StreamingContext(StreamingContextStates.All),vector2Ss);
        surrogateSelector.AddSurrogate(typeof(Vector2Int),new StreamingContext(StreamingContextStates.All),vector2ISs);
        bf.SurrogateSelector = surrogateSelector;
        Data dat = bf.Deserialize (memoryStream) as Data;
        var data = new Data();
        data.CreateDataFromSave(dat);
        return data;
    }

    public static Data saveGame(Data data, string saveName = "proxima_cities.dat") {
        if (!saveName.EndsWith(".dat"))
            saveName += ".dat";
        
        FileStream file = null;
        try
        {
            var dataToSAve = new Data();
            dataToSAve.CreateSaveFromData(data);
            BinaryFormatter bf = new BinaryFormatter();
            SurrogateSelector surrogateSelector = new SurrogateSelector();
            Vector3SerializationSurrogate vector3Ss = new Vector3SerializationSurrogate();
            Vector2SerializationSurrogate vector2Ss = new Vector2SerializationSurrogate();
            Vector2IntSerializationSurrogate vector2ISs = new Vector2IntSerializationSurrogate();
            surrogateSelector.AddSurrogate(typeof(Vector3),new StreamingContext(StreamingContextStates.All),vector3Ss);
            surrogateSelector.AddSurrogate(typeof(Vector2),new StreamingContext(StreamingContextStates.All),vector2Ss);
            surrogateSelector.AddSurrogate(typeof(Vector2Int),new StreamingContext(StreamingContextStates.All),vector2ISs);
            bf.SurrogateSelector = surrogateSelector;
            file = File.Open(Application.persistentDataPath + "/" + saveName, FileMode.OpenOrCreate);
            bf.Serialize (file, dataToSAve);
            file.Close ();
            Debug.Log ("Save completed");
            return dataToSAve;
        }catch (Exception e)
        {
            Debug.LogError(e);
            Debug.LogError("Error cannot save the data");
            file?.Close ();
            return null;
        }
    }

    public static Data Load(string saveName = "proxima_cities.dat") {
        if (!saveName.EndsWith(".dat"))
            saveName += ".dat";
        
        if (!File.Exists(Application.persistentDataPath + "/" + saveName))
        {
            Debug.Log ("No Save Create new");
            return new Data();
        }

        FileStream file = null;
        try 
        {
            BinaryFormatter bf = new BinaryFormatter();
            SurrogateSelector surrogateSelector = new SurrogateSelector();
            Vector3SerializationSurrogate vector3Ss = new Vector3SerializationSurrogate();
            Vector2SerializationSurrogate vector2Ss = new Vector2SerializationSurrogate();
            Vector2IntSerializationSurrogate vector2ISs = new Vector2IntSerializationSurrogate();
            surrogateSelector.AddSurrogate(typeof(Vector3),new StreamingContext(StreamingContextStates.All),vector3Ss);
            surrogateSelector.AddSurrogate(typeof(Vector2),new StreamingContext(StreamingContextStates.All),vector2Ss);
            surrogateSelector.AddSurrogate(typeof(Vector2Int),new StreamingContext(StreamingContextStates.All),vector2ISs);
            bf.SurrogateSelector = surrogateSelector;
            file = File.Open (Application.persistentDataPath + "/" + saveName, FileMode.Open);
            Data dat = bf.Deserialize (file) as Data;
            file.Close ();

            if (dat == null) {
                Debug.Log ("Load fail");
                return new Data ();
            }
            var data = new Data();
            data.CreateDataFromSave(dat);
            Debug.Log ("Load completed");
            data = AddOfflineGain(data);
            return data;
        }
        catch (Exception e)
        {
            Debug.LogError(e);
            Debug.LogError("Error in Save Create new");
            file?.Close ();
            return new Data ();
        }
    }


    private static Data AddOfflineGain(Data data)
    {
        var minuteOffline = data.GetOfflineTime();
        Debug.Log("Offline Minute(s) => " + minuteOffline);
        // Debug.Log("================================");
        foreach (var building in data.GetBuildings())
        {
            var generated = data.GetEnergyByPosition(building.getPosition());
            var needed = building.getEnergyNeeded();
            /*
            Debug.Log($"[{building.getPosition()}] Generated => {generated}");
            Debug.Log($"[{building.getPosition()}] Needed    => {needed}");
            */
            float percentage;
            if (needed == 0 || generated == 0)
            {
                percentage = 0.1f;
            }
            else
            {
                percentage = (float)(Math.Truncate((double)generated / needed * 100) / 100.0);
            }
            if (percentage < 0.1f)
                percentage = 0.1f;
            else if (percentage > 1)
                percentage = 1;
            /*
            Debug.Log($"[{building.getPosition()}] Offline Percentage => {percentage}");
            Debug.Log("================================");
            */
            building.addOfflineGain(percentage, minuteOffline);
        }
        data.GetGreenHouse().greenHouseOfflineGain(Convert.ToInt64(minuteOffline), false);

        return data;
    }
}