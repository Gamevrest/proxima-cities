#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace Tools
{
    class EditorUtils
    {
        public static void DrawUiLine(Color color, int thickness = 2, int padding = 10)
        {
            var r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
            r.height = thickness;
            r.y += padding / 2.0f;
            // r.width -= padding / 2.0f;
            EditorGUI.DrawRect(r, color);
        }
    }
}
#endif