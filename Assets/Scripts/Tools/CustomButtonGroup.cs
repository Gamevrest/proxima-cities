﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Tools
{
    [CreateAssetMenu(fileName = "Create Tablet Button Group")]
    public class CustomButtonGroup : ScriptableObject
    {
        public List<CustomButton> buttonsList;

        public void Awake()
        {
            buttonsList = new List<CustomButton>();
        }

        private void OnEnable()
        {
            buttonsList = new List<CustomButton>();
        }

        private void OnDisable()
        {
            buttonsList = null;
        }

        public void RegisterCustomButton(CustomButton button)
        {
            if (buttonsList.Contains(button))
                return;
            buttonsList.Add(button);
        }

        public void SelectButton(CustomButton button)
        {
            foreach (var customButton in buttonsList.Where(customButton => customButton != button))
            {
                customButton.ForceState(false, false);
            }
        }
    }
}