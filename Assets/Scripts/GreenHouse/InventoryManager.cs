﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    private GreenHouse greenHouse;
    public Transform seedList;
    public GameObject seedElement;
    public Sprite[] spritesRef;
    private Dictionary<SeedType, Sprite> sprites = new Dictionary<SeedType, Sprite>();
    
    private void Start()
    {
        var i = 0;
        foreach (var seedtype in GameManager.AllSeeds)
            sprites.Add(seedtype.Key, spritesRef[i++]);
        greenHouse = GameManager.Instance.GetGreenHouse();
        if (isStorageEmpty())
            greenHouse.addSeed(SeedType.BASIC, 50);
        //greenHouse.addSeed(SeedType.EXPERIMENTAL, 8);
        FillInventory();
    }

    private bool isStorageEmpty()
    {
        var empty = true;
        foreach (var seed in greenHouse.GetSeedStorage())
        {
            if (seed.Value > 0)
                empty = false;
        }

        return empty;
    }

    public void FillInventory()
    {
        foreach (Transform child in seedList) {
            Destroy(child.gameObject);
        }
        foreach (var seed in greenHouse.GetSeedStorage())
        {
            if (seed.Value <= 0) continue;
            var obj = Instantiate(seedElement, seedList);
            obj.GetComponentInChildren<DragSeed>().type = seed.Key;
            obj.GetComponentInChildren<TextMeshProUGUI>().text = "x" + seed.Value;
            obj.GetComponentsInChildren<Image>()[1].sprite = sprites[seed.Key];
        }
    }
}