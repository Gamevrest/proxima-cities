﻿using TMPro;
using UnityEngine;
using Image = UnityEngine.UI.Image;

public class GreenHouseManager : MonoBehaviour
{
    public GameObject[] pots;
    public Sprite[] fruits;
    public Sprite[] plants;
    public Sprite emptySeed;
    public Sprite emptyPlant;
    public Sprite growingPlant;
    public InventoryManager inventory;
    private TextMeshProUGUI[] timers;
    private GreenHouse greenHouse;
    private int[] fruitToColelct = new int[3];
    void Start()
    {
        EventManager.Instance.Subscribe(AddedSeedEvent);
        EventManager.Instance.Subscribe(UpdateTimer);
        EventManager.Instance.Subscribe(PlantCollected);
        EventManager.Instance.Subscribe(DisplayFruits);
        EventManager.Instance.Subscribe(EnableGreenHouse);
        EventManager.Instance.Subscribe(DisableGreenHouse);
        greenHouse = GameManager.Instance.GetGreenHouse();
        CheckPots();
    }

    private void CheckPots()
    {
        var i = 0;
        foreach (var pot in pots)
        {
            var potAtIndex = greenHouse.GetPotAtIndex(i);
            if (potAtIndex != null)
            {
                var seedIndex = 0;
                switch (potAtIndex.getSeedType()) // j'ai refait les stats très vite fait ici pour le random du fruit généré
                {                            // y a pas les mutation psk j'ai pas de moyen simple de savoir si a coté c'est fini et si y a qqchose a coté etc meme si toi tu le fait ptet??
                    case SeedType.BASIC :
                        fruitToColelct[i] = Random.Range(0, 3) < 1 ? 1 : 0;
                        seedIndex = 0;
                        break;
                    case SeedType.ADVANCED :
                        fruitToColelct[i] = Random.Range(0, 3) < 1 ? 2 : 1;
                        seedIndex = 1;
                        break;
                    case SeedType.EXPERIMENTAL :
                        fruitToColelct[i] = Random.Range(0, 3) < 1 ? 3 : 2;
                        seedIndex = 2;
                        break;
                    case SeedType.TWISTED :
                        fruitToColelct[i] = Random.Range(0, 2) < 1 ? 3 : 2;
                        seedIndex = 3;
                        break;
                }
                pots[i].transform.Find("Fruit1").gameObject.SetActive(true);
                pots[i].transform.Find("Fruit1").transform.GetComponentInChildren<Image>().sprite = fruits[fruitToColelct[i]];
                pots[i].transform.Find("Plant").transform.GetComponentInChildren<Image>().sprite = plants[seedIndex];
            }
            i++;
        }
    }

    private void AddedSeedEvent(SeedPlantedEvent evt)
    {
        pots[evt.getPotIndex()].transform.Find("Plant").transform.GetComponentInChildren<Image>().sprite = growingPlant;
        inventory.FillInventory();
    }
    
    private void DisplayFruits(SeedFinishGrowEvent evt)
    {
        var seedIndex = 0;
        var potIndex = evt.getPotIndex();
        switch (evt.getSeedType()) // j'ai refait les stats très vite fait ici pour le random du fruit généré
        {                            // y a pas les mutation psk j'ai pas de moyen simple de savoir si a coté c'est fini et si y a qqchose a coté etc meme si toi tu le fait ptet??
            case SeedType.BASIC :
                fruitToColelct[potIndex] = Random.Range(0, 3) < 1 ? 1 : 0;
                seedIndex = 0;
                break;
            case SeedType.ADVANCED :
                fruitToColelct[potIndex] = Random.Range(0, 3) < 1 ? 2 : 1;
                seedIndex = 1;
                break;
            case SeedType.EXPERIMENTAL :
                fruitToColelct[potIndex] = Random.Range(0, 3) < 1 ? 3 : 2;
                seedIndex = 2;
                break;
            case SeedType.TWISTED :
                fruitToColelct[potIndex] = Random.Range(0, 2) < 1 ? 3 : 2;
                seedIndex = 3;
                break;
        }
        pots[potIndex].transform.Find("Fruit1").gameObject.SetActive(true);
        pots[potIndex].transform.Find("Fruit1").transform.GetComponentInChildren<Image>().sprite = fruits[fruitToColelct[potIndex]];
        pots[potIndex].transform.Find("Plant").transform.GetComponentInChildren<Image>().sprite = plants[seedIndex];
    }
    
    private void PlantCollected(PlantCollectedEvent evt)
    {
        greenHouse.addSeed((SeedType)fruitToColelct[evt.getPotIndex()], 2);
        pots[evt.getPotIndex()].transform.Find("Seed").transform.GetComponentInChildren<Image>().sprite = emptySeed;
        pots[evt.getPotIndex()].transform.Find("Plant").transform.GetComponentInChildren<Image>().sprite = emptyPlant;
        inventory.FillInventory();
    }

    void UpdateTimer(GameLoopEvent evt)
    {
        for (int i = 0; i < pots.Length; i++)
        {
            var pot = GameManager.Instance.GetGreenHouse().GetPotAtIndex(i);
            if (pot != null)
                pots[i].GetComponentInChildren<TextMeshProUGUI>().text = Format(pot.getRemainingTime());
            else
                pots[i].GetComponentInChildren<TextMeshProUGUI>().text = "";
        }
    }

    string Format(long nb)
    {
        return nb / 60 + ":" + ((nb % 60 < 10) ? "0" : "") + nb % 60;
    }

    private void DisableGreenHouse(SwitchTo2DEvent e)
    {
        foreach (var pot in pots)
        {
            pot.SetActive(false);
        }
    }

    private void EnableGreenHouse(SwitchToGreenHouseEvent e)
    {
        foreach (var pot in pots)
        {
            pot.SetActive(true);
        }
    }
}