﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Fruit : MonoBehaviour, IPointerClickHandler
{
    public int potIndex;
    
    public void OnPointerClick(PointerEventData eventData)
    {
        print("MOUSE DOWN ON " + potIndex);
        EventManager.Instance.Raise(new CollectPlantEvent().setPotIndex(potIndex));
        gameObject.SetActive(false);
    }
}
