﻿using UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DropSeed : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    private BuildingsAbstract building;
    private Vector2Int buildingPos;
    public Image containerImage;
    private Sprite dropSprite;
    public EventManager eventManager = EventManager.Instance;
    [HideInInspector] public bool hasUpgrade = false;
    public Color highlightColor = Color.yellow;
    public int index;
    [HideInInspector] public bool isEmpty = true;

    private Color normalColor;
    public Image receivingImage;
    private SeedType seedType;

    private PointerEventData tmpdata; // QUICK FIX OnEndDrag(set up)

    private WindowManager windowManager;

    public void OnDrop(PointerEventData data)
    {
        containerImage.color = normalColor;

        if (receivingImage == null)
            return;

        dropSprite = GetDropSprite(data);
        EventManager.Instance.Raise(new PlantSeedEvent().setPotIndex(index).setSeedType(seedType));
        /*if (dropSprite != null)
        {
            receivingImage.sprite = dropSprite;
        }*/
    }

    public void OnPointerEnter(PointerEventData data)
    {
        if (containerImage == null)
            return;

        Sprite dropSprite = GetDropSprite(data);
        if (dropSprite != null)
            containerImage.color = highlightColor;
    }

    public void OnPointerExit(PointerEventData data)
    {
        if (containerImage == null)
            return;

        containerImage.color = normalColor;
    }

    private void Start()
    {
        eventManager.Subscribe(AddedSeedEvent);
    }

    public void InitBuilding(BuildingsAbstract actualBuilding)
    {
//u init c bien
    }

    public void RemoveBuilding()
    {
        isEmpty = true;
        building = null;
        receivingImage.sprite = UI.Resources.Load<Sprite>("UI/blank");
    }

    private void GetErrorEmpty(GameErrorEvent evt)
    {
        var t = evt.GetException() as NotEmptyPositionException;
        if (t != null)
        {
            print(evt.GetMessageId());
            eventManager.UnSubscribe(GetErrorEmpty);
        }
    }

    private void AddedSeedEvent(SeedPlantedEvent evt)
    {
        if (evt.getPotIndex() == index)
        {
            evt.getSeedType();
            if (dropSprite != null)
            {
                receivingImage.sprite = dropSprite;
                tmpdata.pointerDrag.GetComponent<DragSeed>()
                    .OnEndDrag(tmpdata); // QUICK FIX OnEndDrag n'est plus appelé dans DragSeed, alors je l'apelle comme ca :shrug:
            }
        }
    }

    public void OnEnable()
    {
        if (containerImage != null)
            normalColor = containerImage.color;
    }

    private Sprite GetDropSprite(PointerEventData data)
    {
        var originalObj = data.pointerDrag;
        if (originalObj == null)
            return null;

        var dragMe = originalObj.GetComponent<DragSeed>();
        if (dragMe == null)
            return null;
        seedType = dragMe.type;

        var srcImage = originalObj.GetComponent<Image>();
        if (srcImage == null)
            return null;
        tmpdata = data; // QUICK FIX OnEndDrag(set up)
        return srcImage.sprite;
    }

    public void SetBuilding(BuildingsAbstract build)
    {
        building = build;
    }

    public BuildingsAbstract GetBuilding()
    {
        return building;
    }

    public void SetPos(Vector2Int pos)
    {
        buildingPos = pos;
    }

    public Vector2 GetPos()
    {
        return buildingPos;
    }

    public void SetSeedType(SeedType type)
    {
        seedType = type;
    }

    public SeedType GetBuildingType()
    {
        return seedType;
    }
}