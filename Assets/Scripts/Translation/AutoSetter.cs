﻿using System;
using System.Collections.Generic;
using Event;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Translation
{
    [Serializable]
    public class AutoSetter
    {
        [ReadOnly] public string id;
        [ReadOnly] public TextMeshProUGUI uGui;
        [ReadOnly] public object[] vars;

        public AutoSetter(string id, TextMeshProUGUI uGui, params object[] inpVars)
        {
            this.id = id;
            this.uGui = uGui;
            vars = inpVars;
            EventManager.Instance.Subscribe(LanguageChanged);
            RefreshText();
        }

        ~AutoSetter()
        {
            EventManager.Instance.UnSubscribe(LanguageChanged);
        }

        private void LanguageChanged(ChangeLanguageEvent evt)
        {
            RefreshText();
        }

        public void RefreshText()
        {
            if (uGui != null)
                uGui.text = Tr.Get(id, vars);
        }

        public void SetId(string newId)
        {
            id = newId;
            RefreshText();
        }

        public void SetTextComponent(TextMeshProUGUI newUGui)
        {
            uGui = newUGui;
            RefreshText();
        }

        public void SetVars(params object[] newVars)
        {
            vars = newVars;
            RefreshText();
        }
    }
}