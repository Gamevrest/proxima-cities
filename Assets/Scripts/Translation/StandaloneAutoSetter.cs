﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Translation
{
    public class StandaloneAutoSetter : MonoBehaviour
    {
        public string id;
        private TextMeshProUGUI _uGui;
        private AutoSetter _setter;

        private void Start()
        {
            if (!_uGui) 
                _uGui = GetComponent<TextMeshProUGUI>();
            _setter  = new AutoSetter(id, _uGui);
        }
    }
}