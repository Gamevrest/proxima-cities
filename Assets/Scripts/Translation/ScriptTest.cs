﻿using System;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;

namespace Translation
{
    public class ScriptTest : MonoBehaviour
    {
        public string id;
        private TextMeshProUGUI _uGui;
        private AutoSetter _setter;

        private void Start()
        {
            if (_uGui == null)
                _uGui = GetComponent<TextMeshProUGUI>();
            _setter = new AutoSetter(id, _uGui, Time.deltaTime);

            
        }

        private void Update()
        {
            _setter.SetVars(Time.deltaTime);
        }
        
        
    }
}