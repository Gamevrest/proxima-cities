﻿using System;
using TMPro;
using Tools;
using UnityEngine;
using UnityEngine.UI;

namespace Translation
{
    public class DebugSwitcher : MonoBehaviour
    {
        public float spacing;
        public GameObject prefab;
        public Transform container;

        private void Start()
        {
           // Utils.CleanChildren(container);
            var prefSize = prefab.GetComponent<RectTransform>().sizeDelta.y;
            foreach (Tr.Languages language in Tr.GetLanguages())
            {
                var a = Instantiate(prefab, container);
                a.name = $"{language}_button";
                a.GetComponent<Button>().onClick.AddListener(() => Tr.SelectLanguage(language));
                a.GetComponentInChildren<TextMeshProUGUI>().text = language.ToString();
            }
        }
    }
}