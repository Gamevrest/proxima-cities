﻿using UnityEngine;
#if UNITY_EDITOR
using Tools;
using UnityEditor;

#endif
namespace Translation
{
#if UNITY_EDITOR

    public class TranslationEditor : EditorWindow
    {
        private Vector2 _scroll;
    
        [MenuItem("Window/Translation Editor")]
        public static void ShowWindow()
        {
            GetWindow(typeof(TranslationEditor), false, "Translation Editor");
        }

        private void OnGUI()
        {
            _scroll = GUILayout.BeginScrollView(_scroll);
            //DrawVerticalArray();
            DrawHorizontalArray();
            EditorGUILayout.Separator();
            GUILayout.EndScrollView();
        }

        private void DrawHorizontalArray()
        {
            GUILayout.BeginHorizontal();
            foreach (Tr.Languages language in Tr.GetLanguages())
                if (GUILayout.Button(language.ToString()))
                    Tr.SelectLanguage(language);

            GUILayout.EndHorizontal();

            foreach (var entry in Tr.GetEntries())
            {
                EditorUtils.DrawUiLine(Color.black, 1, 0);
                GUILayout.BeginHorizontal();
                foreach (Tr.Languages language in Tr.GetLanguages())
                    GUILayout.Label(entry.GetTranslations()[language]);
                GUILayout.EndHorizontal();
            }
        }

        private void DrawVerticalArray()
        {
            GUILayout.BeginHorizontal();

            foreach (Tr.Languages language in Tr.GetLanguages())
            {
                GUILayout.BeginVertical();
                if (GUILayout.Button(language.ToString()))
                    Tr.SelectLanguage(language);
                foreach (var entry in Tr.GetEntries())
                {
                    EditorUtils.DrawUiLine(Color.black, 1, 0);
                    GUILayout.Label(entry.GetTranslations()[language]);
                }

                GUILayout.EndVertical();
            }

            GUILayout.EndHorizontal();
        }
    }
#endif
}