﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Event;
using UnityEngine;

namespace Translation
{
    [Serializable]
    public sealed class Tr
    {
        public enum Languages
        {
            ID,
            anglais,
            francais
        }

        public static Tr Instance { get; } = new Tr();

        private Languages _selectedLanguage = Languages.anglais;
        private List<Entry> _data;
        private List<string> _languages = new List<string> {"Id", "anglais", "francais"};

        private const string CachedPath = "cachedTranslation";
        private string _savedPath = "translation";
        public static bool hasFinished;

        private Tr()
        {
        }

        static Tr()
        {
            hasFinished = true;
            Instance._savedPath = $"{Application.persistentDataPath}/{Instance._savedPath}.csv";
            Instance._data = new List<Entry>();
            Instance.LoadFromFile();
            if (Instance._data.Count <= 0)
                Instance.LoadCached();
            Instance.LoadFromDatabase();
            SelectLanguage(Languages.francais);
        }

        private string Get(string id)
        {
            foreach (var entry in _data)
                if (entry.GetKey() == id)
                {
                    var tr = entry.GetValue(_selectedLanguage);
                    if (string.IsNullOrWhiteSpace(tr))
                    {
                        SendToDatabase(entry);
                        tr = id;
                    }

                    return tr;
                }

            var newEntry = new Entry(id);
            _data.Add(newEntry);
            SendToDatabase(newEntry);
            return newEntry.GetValue(_selectedLanguage);
        }

        private void MajEntry(string id, Languages lng, string data)
        {
            Entry tmp = null;
            var exists = false;
            foreach (var entry in _data)
                if (entry.GetKey() == id)
                {
                    tmp = entry;
                    exists = true;
                }

            if (tmp == null)
                tmp = new Entry(id);
            tmp.SetTranslation(lng, data.Replace("\r", "").Replace("\"", ""));
            if (!exists && tmp.IsValid())
                _data.Add(tmp);
        }

        public static string Get(string id, params object[] inpVars)
        {
            var input = Instance.Get(id);
            var i = 0;
            input = Regex.Replace(input, "%.", m => ("{" + i++ + "}"));
            return string.Format(input, inpVars);
        }

        public static Languages GetLanguage() => Instance._selectedLanguage;

        public static Array GetLanguages() => Enum.GetValues(typeof(Languages));

        public static IEnumerable<Entry> GetEntries() => Instance._data;

        public static void SelectLanguage(Languages lng)
        {
            Instance._selectedLanguage = lng;
            EventManager.Instance.Raise(new ChangeLanguageEvent(lng));
        }

        public void LoadFromFile()
        {
            var path = _savedPath;
            if (!File.Exists(path))
                File.Create(path).Close();
            var rows = File.ReadAllLines(path);
            LoadFromArray(rows);
        }

        public void LoadCached()
        {
            var path = CachedPath;
            Reset();
            var file = Resources.Load<TextAsset>(path);
            if (file == null)
            {
                Debug.LogError($"Unable to load Resource {path}");
                return;
            }
            Debug.Log($"Load from {path}");
            var rows = file.text.Split('\n');
            LoadFromArray(rows);
        }

        private void LoadFromArray(string[] rows)
        {
            foreach (var row in rows)
            {
                var tmp = row.Replace("<br>", "\n").Split(',');
                if (row == rows.First()) continue;
                var id = tmp.First();
                var i = 0;
                foreach (Languages language in GetLanguages())
                {
                    MajEntry(id, language, tmp[i]);
                    i++;
                }
            }
        }

        private void LoadFromDatabase()
        {
            // FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://proxima-cities-8122097.firebaseio.com/");
            // var db = FirebaseDatabase.DefaultInstance.RootReference;
            // db.GetValueAsync().ContinueWith(task =>
            // {
            //     if (task.IsFaulted){
            //         DevConsole.Log($"Firebase db FAULT : \n{task.Exception}");
            //         hasFinished = true;
            //     }
            //     else if (task.IsCompleted)
            //     {
            //         var translation = task.Result.Child("translation");
            //         DevConsole.Log($"fetched {translation.ChildrenCount} translations");
            //         foreach (var row in translation.Children)
            //         {
            //             var id = row.Key;
            //             MajEntry(id, Languages.ID, id);
            //             MajEntry(id, Languages.anglais, row.Child("anglais").Value.ToString());
            //             MajEntry(id, Languages.francais, row.Child("francais").Value.ToString());
            //         }
            //
            //         ExportToFile();
            //         SelectLanguage(Languages.francais);
            //     }
            //     else if (task.IsCanceled)
            //     {
            //         DevConsole.Log($"Firebase db CANCELED : \n{task.Exception}");
            //         hasFinished = true;
            //     }
            // });
        }

        private void SendToDatabase(Entry entry)
        {
            // FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://proxima-cities-8122097.firebaseio.com/");
            // var db = FirebaseDatabase.DefaultInstance.RootReference;
            // var entryKey = db.Child("newTranslation").Child(entry.GetKey());
            // foreach (var translation in entry.GetTranslations())
            // {
            //     if (translation.Key == Languages.ID) continue;
            //     entryKey.Child(translation.Key.ToString()).SetValueAsync(translation.Value).ContinueWith(task =>
            //     {
            //         if (task.IsFaulted)
            //             DevConsole.Log($"send data FAULT : \n{task.Exception}");
            //         else if (task.IsCanceled)
            //             DevConsole.Log($"send data canceled : \n{task.Exception}");
            //     });
            // }
        }

        private void ExportToFile()
        {
            var path = _savedPath;
            if (!File.Exists(path))
                File.Create(path).Close();
            using (var wf = new StreamWriter(path, false))
            {
                wf.WriteLine($"{string.Join(",", Enum.GetNames(typeof(Languages)))}");
                foreach (var entry in _data)
                    if (entry.IsValid())
                        wf.WriteLine(entry.ToString().Replace("\n", "<br>"));
            }

            SelectLanguage(Languages.francais);
            hasFinished = true;
        }

        public void Reset()
        {
            _data = new List<Entry>();
            _languages = new List<string>();
        }

        public Languages StringToLanguage(string s)
        {
            foreach (Languages lng in GetLanguages())
                if (s == lng.ToString())
                    return lng;
            return Languages.ID;
        }
    }
}