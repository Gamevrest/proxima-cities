using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace AutoBuild
{
    public class Builder: MonoBehaviour
    {
        public static void PerformBuild()
        {
            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
            buildPlayerOptions.scenes = new[] { "Assets/Scenes/Menus.unity", "Assets/Scenes/Core.unity", "Assets/Scenes/2D.unity", "Assets/Scenes/NewAr.unity", "Assets/Scenes/Serre.unity"  };
            buildPlayerOptions.locationPathName = "./proxima-cities.apk";
            buildPlayerOptions.target = BuildTarget.Android;
            buildPlayerOptions.options = BuildOptions.Development;

            BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
            BuildSummary summary = report.summary;


            switch (summary.result)
            {
                case BuildResult.Succeeded:
                    Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
                    break;
                case BuildResult.Failed:
                    Debug.Log("Build failed");
                    break;
            }
        }
    }
}