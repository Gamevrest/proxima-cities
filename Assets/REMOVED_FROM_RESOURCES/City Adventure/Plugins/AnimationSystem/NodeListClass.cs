﻿using UnityEngine;

[System.Serializable]
public struct NodeListClass{
	public GameObject Node;
	public float Speed;
}
